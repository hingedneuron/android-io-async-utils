package com.hn.android.io.fs;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public interface FileSystem
{
	public boolean isVolumeReadable();
	public boolean isVolumeWritable();

	public File rootDir();
	public File tempDir();

	/**
	 * Create or obtain specified directory.
	 */
	public File getDir(File baseDir, String subDir);
	public File getFile(File dir, String name);

	public FileInputStream readFile(File dir, String file);
	public FileOutputStream writeFile(File dir, String file);

	public File[] listFiles(File dir, String... ext);

	public long copyFile(File from, File to);

	public boolean deleteFile(File dir, String name);
	public boolean deleteDir(File dir);

	public Closeable openFile(File dir, String name, boolean forWriting);
}
