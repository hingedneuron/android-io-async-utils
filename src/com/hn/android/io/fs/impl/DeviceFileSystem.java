package com.hn.android.io.fs.impl;

import java.io.File;

import android.content.Context;

import com.hn.android.io.fs.FS;

public class DeviceFileSystem extends DefaultFileSystem
{
	public DeviceFileSystem(Context con)
	{
		super(con);
	}

	@Override
	public boolean isVolumeReadable()
	{
		return true;
	}

	@Override
	public boolean isVolumeWritable()
	{
		return true;
	}

	@Override
	public File rootDir()
	{
		return _context.getFilesDir().getParentFile();
	}

	@Override
	public File tempDir()
	{
		return _context.getCacheDir();
	}

	@Override
	public File picsDir()
	{
		return getDir(getAppName());
	}

	@Override
	public File getDir(String dir)
	{
		return _context.getDir(dir, FS.DEFAULT_MODE);
	}
}
