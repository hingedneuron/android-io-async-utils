package com.hn.android.io.fs.impl;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.hn.android.io.fs.FS;
import com.hn.android.io.fs.FileSystem;

/**
 * Class which implements the basic FileSystem interface and
 * provides much expanded additional functionality. Methods
 * which are SD- or device-filesystem dependent are left
 * unimplemented here.
 */
public abstract class DefaultFileSystem implements FileSystem
{
	/*
	 * File objects pointing to filesystem directories.
	 * DL and TEMP and self-explanatory. DATA is intended
	 * to hold persistent data. Creation of the TEMP and
	 * PICS directories depend on filesystem type and are
	 * delegated to subclasses.
	 */
	public final File	DIR_DL, DIR_TEMP,
						DIR_DATA, DIR_PICS;

	protected final Context _context;

	public DefaultFileSystem(Context con)
	{
		_context = con;

		DIR_DL = getDir(FS.DIR_DL);
		DIR_TEMP = tempDir();

		DIR_DATA = getDir(FS.DIR_DATA);
		DIR_PICS = picsDir();
	}

	protected String getAppName()
	{
		return (String)_context.getPackageManager()
			.getApplicationLabel(_context.getApplicationInfo());
	}

	/**
	 * Creating / getting root-directory
	 * folder is a specialised function.
	 */
	public abstract File getDir(String name);

	/**
	 * Creating / getting pics directory
	 * folder is a specialised function.
	 */
	public abstract File picsDir();

	@Override
	public File getDir(File baseDir, String subDir)
	{
		baseDir = (baseDir == null ? rootDir() : baseDir);

		if(isVolumeReadable() && baseDir.equals(rootDir()))
			return getDir(subDir);
		else if(isVolumeWritable())
		{
			File newDir = getFile(baseDir, subDir);
			newDir.mkdirs();

			return newDir;
		}

		return null;
	}

	public File getTempSubDir(String dir)
	{
		return getDir(tempDir(), dir);
	}

	@Override
	public File getFile(File dir, String name)
	{
		return new File(dir, name);
	}

	public File getTempFile(String name)
	{
		return getFile(tempDir(), name);
	}

	public FileInputStream readFile(String name)
	{
		return readFile(null, name);
	}

	@Override
	public FileInputStream readFile(File dir, String name)
	{
		return (FileInputStream)openFile(dir, name, false);
	}

	public FileOutputStream writeFile(String name)
	{
		return writeFile(null, name);
	}

	@Override
	public FileOutputStream writeFile(File dir, String name)
	{
		return (FileOutputStream)openFile(dir, name, true);
	}

	public long copyFile(InputStream from, File to)
	{
		long sum = 0;

		try
		{
			OutputStream out = new FileOutputStream(to);

			// transfer bytes
			byte[] buf = new byte[1024];
			int len;

			while((len = from.read(buf)) > 0)
			{
				out.write(buf, 0, len);
				sum += len;
			}

			out.flush();

			from.close();
			out.close();
		}
		catch(Exception e)
		{
			Log.e(FS.TAG, e.getLocalizedMessage(), e);
		}

		return sum;
	}

	public void copyFiles(File toDir, File... files)
	{
		for(File from : files)
		{
			File to = getFile(toDir, from.getName());
			copyFile(from, to);
		}
	}

	@Override
	public long copyFile(File from, File to)
	{
		try
		{
			return copyFile(new FileInputStream(from), to);
		}
		catch(Exception e)
		{
			Log.e(FS.TAG, e.getLocalizedMessage(), e);
		}

		return 0;
	}

	public File[] listFiles(String dir)
	{
		return listFiles(null, dir, null);
	}

	@Override
	@SuppressLint("DefaultLocale")
	public File[] listFiles(File dir, final String... exts)
	{
		return dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name)
			{
				if(exts == null)
					return true;

				for(String ext : exts)
				{
					if(name.toLowerCase().endsWith(ext.toLowerCase()))
						return true;
				}

				return false;
			}
		});
	}

	public boolean deleteFile(String name)
	{
		return deleteFile(null, name);
	}

	public void deleteFiles(File... files)
	{
		for(File file : files)
			file.delete();
	}

	@Override
	public boolean deleteFile(File dir, String name)
	{
		return getFile((dir == null ?
			rootDir() : dir), name).delete();
	}

	@Override
	public boolean deleteDir(File dir)
	{
		return deleteDir(dir, false);
	}

	public boolean deleteDir(File dir, boolean onlycontents)
	{
		if(dir == null)
			return false;

		for(File f : dir.listFiles())
		{
			if(f.isDirectory())
				deleteDir(f);
			else
				f.delete();
		}

		return (onlycontents || dir.delete());
	}

	@Override
	public Closeable openFile(File dir, String name, boolean forWriting)
	{
		try
		{
			dir = (dir == null ? rootDir() : dir);

			if(forWriting)
				return new FileInputStream(getFile(dir, name));
			else
				return new FileOutputStream(getFile(dir, name));
		}
		catch(Exception e)
		{
			Log.e(FS.TAG, e.getLocalizedMessage(), e);
			return null;
		}
	}

	public void cleanup()
	{
		if(isVolumeWritable())
		{
			deleteDir(DIR_TEMP, true);
			deleteDir(DIR_DL, true);
		}
	}
}
