package com.hn.android.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class UtilIO
{
	public static int pipe(InputStream input, OutputStream output) throws IOException
	{
		byte[] buffer = new byte[1024];
		int read, size = 0;

		while((read = input.read(buffer)) >= 0)
		{
			output.write(buffer, 0, read);
			size += read;
		}

		return size;
	}

	public static int pipeAndClose(InputStream input, OutputStream output) throws IOException
	{
		int size = pipe(input, output);

		input.close();
		output.close();

		return size;
	}
}
