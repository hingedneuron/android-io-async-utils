package com.hn.android.io.dl;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.hn.android.async.AS;
import com.hn.android.async.Callback;
import com.hn.android.async.GenericTask;
import com.hn.android.async.impl.DownloadCallback;
import com.hn.android.async.impl.UnzipCallback;
import com.hn.android.io.fs.FS;

public class DL extends BroadcastReceiver
{
	public static final String TAG = "android-io-async-utils DL";

	private static final HashMap<Long, Callback>
		callbacks = new HashMap<Long, Callback>();

	private static final DL RECEIVER = new DL();

	private static DownloadManager dlManager;

	private static final Object NULL = null;

	private static Context _context;

	public static void setContext(Context con)
	{
		_context = con;

		dlManager = (DownloadManager)
			_context.getSystemService(Context.DOWNLOAD_SERVICE);

		_context.registerReceiver(RECEIVER,
            new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
	}

	public static DownloadManager dlmgr()
	{
		return dlManager;
	}

	/*
	 * Download the file using URLConnection and an InputStream. On
	 * some platforms (e.g. Samsung Note 2) the BroadcastReceiver is
	 * flaky; this is a less sophisticated but more reliable alternative.
	 */
	public static GenericTask downloadRaw(String url, File dlDir, final Callback cb)
	{
		try
		{
			URL source = new URL(url);

			final File dest = new File(dlDir,
				new File(source.getPath()).getName());

			// spawn async thread to download in background: pre, bg, post, args
			return AS.spawn(null, new DownloadCallback(), cb, source, dest);
		}
		catch(Exception e)
		{
			Log.e(TAG, e.getLocalizedMessage(), e);
		}

		return null;
	}

	/*
	 * This method uses the BroadcastReceiver service to download, so
	 * the download will appear in the Android tray.
	 */
	public static long downloadService(String url, File dlDir, Callback cb)
	{
		if(_context == null)
			return Long.MIN_VALUE;

		try
		{
			URL _url = new URL(url);
			Uri uri = Uri.parse(url);

			File dest = new File(dlDir,
				new File(_url.getPath()).getName());

			Request mReq = new Request(uri)
				.setTitle(dest.getName())
				.setDestinationUri(Uri.fromFile(dest));

			long id = Long.MIN_VALUE;

			synchronized(callbacks)
			{
				id = dlManager.enqueue(mReq);
				callbacks.put(id, cb);
			}

			return id;
		}
		catch(Exception e)
		{
			Log.e(TAG, e.getLocalizedMessage(), e);
		}

		return Long.MIN_VALUE;
	}

	/* A bit complicated; download is done using raw download method,
	 * then in the download callback an async task is spawned to do the
	 * unzipping, and finally the user-supplied callback is invoked
	 * on the main app thread. The zip file is downloaded to the
	 * default download folder of the fs() filesystem; that is,
	 * the SD card if writable, otherwise internal storage.
	 */
	public static void downloadAndUnzip(String url, final File unzipDir, final Callback cb)
	{
		downloadAndUnzip(url, unzipDir, FS.wfs().DIR_DL, cb);
	}

	/* Specify the download directory explicitly. */
	public static void downloadAndUnzip(String url, final File unzipDir, File dlDir, final Callback cb)
	{
		downloadRaw(url, dlDir, new Callback() {
			@Override
			public Object call(Object caller, Object... args)
			{
				final File dlfile =
					(args[0] == null ? null : (File)args[0]);

				if(dlfile != null)
					AS.spawn(null, new UnzipCallback(), cb, dlfile, unzipDir);
				else if(cb != null)
					cb.call(this, NULL);

				return dlfile;
			}
		});
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		if(_context == null)
			_context = context;

		Set<Long> keys = null;

		synchronized(callbacks)
		{
			keys = callbacks.keySet();
		}

		long[] ids = new long[keys.size()];
		int i = 0;

		for(Long key : keys)
			ids[i++] = key;

		DownloadManager.Query query =
			new DownloadManager.Query().setFilterById(ids);

		Cursor info = dlManager.query(query);

		int	col_id = info.getColumnIndex(DownloadManager.COLUMN_ID),
			col_status = info.getColumnIndex(DownloadManager.COLUMN_STATUS),
			col_uri = info.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);

		// invoke user-supplied callback
		while(info.moveToNext())
		{
			long id = info.getLong(col_id);

			Uri dlUri =
				Uri.parse(info.getString(col_uri));

			File downloadFile =
				new File(dlUri.getPath());

			Callback cb = callbacks.remove(id);

			if(cb != null)
			{
				int status =
					info.getInt(col_status);

				cb.call(intent, downloadFile, status);
			}
		}
	}

	public static void cleanup()
	{
		if(_context != null)
			_context.unregisterReceiver(RECEIVER);
	}
}
