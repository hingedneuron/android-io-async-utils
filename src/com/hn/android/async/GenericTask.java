package com.hn.android.async;

import android.os.AsyncTask;
import android.os.Process;

public class GenericTask extends AsyncTask<Object, String, Object>
{
	protected final Callback _pre, _work, _post;

	public GenericTask()
	{
		this(null, null, null);
	}

	public GenericTask(GenericTask other)
	{
		this(other._pre, other._work, other._post);
	}

	public GenericTask(Callback work)
	{
		this(null, work, null);
	}

	public GenericTask(Callback pre, Callback work, Callback post)
	{
		_pre = pre;
		_work = work;
		_post = post;
	}

	@Override
	protected void onPreExecute()
	{
		if(_pre != null)
			_pre.call(this);
	}

	@Override
	protected Object doInBackground(Object... args)
	{
		Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);

		if(_work != null)
			return _work.call(this, args);

		return null;
	}

	@Override
	protected void onPostExecute(Object result)
	{
		if(_post != null)
			_post.call(this, result);
	}

	// surface progress update method
	public void publish(String... progress)
	{
		publishProgress(progress);
	}
}
