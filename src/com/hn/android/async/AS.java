package com.hn.android.async;

public class AS
{
	public static GenericTask create(Callback pre,
		Callback work, Callback post)
	{
		return new GenericTask(pre, work, post);
	}

	public static GenericTask spawn(Callback pre,
		Callback work, Callback post, Object... args)
	{
		GenericTask task = create(pre, work, post);
		task.execute(args);

		return task;
	}
}
