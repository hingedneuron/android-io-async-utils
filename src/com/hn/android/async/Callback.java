package com.hn.android.async;

public interface Callback
{
	public Object call(Object caller, Object... args);
}
