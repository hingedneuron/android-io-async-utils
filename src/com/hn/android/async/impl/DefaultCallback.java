package com.hn.android.async.impl;

import com.hn.android.async.Callback;

// allow a set of private params on instantiation
public abstract class DefaultCallback implements Callback
{
	protected final Object[] _privateParams;

	public DefaultCallback(Object... privateParams)
	{
		_privateParams = privateParams;
	}
}
