package com.hn.android.async.impl;

import java.io.File;

import com.hn.android.io.Zip;

public class UnzipCallback extends DefaultCallback
{
	private static final int ZIP_SRC = 0, ZIP_DEST = 1;

	@Override
	public Object call(Object caller, Object... args)
	{
		if(args == null || args.length == 0)
			return null;

		File zipFile = (File)args[ZIP_SRC];

		File zipDir = (File)(args.length > 1 ?
			args[ZIP_DEST] : zipFile.getParentFile());

		Zip.unpackZip(zipFile, zipDir);

		return zipDir;
	}
}
