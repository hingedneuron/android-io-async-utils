package com.hn.android.async.impl;

import java.io.File;
import java.net.URL;

import com.hn.android.async.GenericTask;
import com.hn.android.http.HttpUtils;

public class DownloadCallback extends DefaultCallback
{
	public static final String	MSG_ERROR = "Download Error";

	@Override
	public Object call(Object caller, Object... args)
	{
		URL source = (URL)args[0];
		File dest = (File)args[1];

		return HttpUtils.getFile
			(source.toString(), dest, (GenericTask)caller);
	}
}
