package com.hn.android.http;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.util.Log;

import com.hn.android.async.GenericTask;
import com.hn.android.io.dl.DL;

public class HttpUtils
{
	private static final int CONNECTION_TIMEOUT = 5000;
	private static final int SOCKET_TIMEOUT = 5000;

	public static File getFile(String url, File dest, GenericTask task)
	{
		try
		{
			return getFile(new URL(url), dest, task);
		}
		catch(Exception e)
		{
			Log.e(DL.TAG, e.getLocalizedMessage(), e);
		}

		return null;
	}

	public static File getFile(URL url, File dest, GenericTask caller)
	{
		if(url != null && dest != null)
		{
			HttpURLConnection con = null;

			try
			{
				con = connect(url);

				InputStream in =
					new BufferedInputStream(con.getInputStream());

				FileOutputStream fos =
					new FileOutputStream(dest);

				int lengthOfFile =
					con.getContentLength();

				byte[] buffer = new byte[4096];

				int total = 0, count = 0;

				while((count = in.read(buffer)) > 0)
				{
					caller.publish(dest.getName(),
						String.valueOf(((total+=count) / lengthOfFile) * 100));

					fos.write(buffer, 0, count);
				}

				fos.flush();

				fos.close();
				in.close();

				con.disconnect();

				return dest;
			}
			catch(Exception e)
			{
				if(con != null)
					con.disconnect();
			}
		}

		return null;
	}

	public static long readLastModified(String url)
	{
		HttpURLConnection con = null;

		try
		{
			con = connect(new URL(url));

			long modTime =
				con.getLastModified();

			con.disconnect();

			return modTime;
		}
		catch(Exception e)
		{
			Log.e(DL.TAG, e.getLocalizedMessage(), e);

			if(con != null)
				con.disconnect();
		}

		return 0;
	}

	private static HttpURLConnection connect(URL url) throws IOException
	{
		URLConnection connection = url.openConnection();

		connection.setConnectTimeout(CONNECTION_TIMEOUT);
		connection.setReadTimeout(SOCKET_TIMEOUT);

		return (HttpURLConnection)connection;
	}
}
